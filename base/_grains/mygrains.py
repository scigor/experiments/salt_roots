#!/usr/bin/python3

import socket
import logging

log = logging.getLogger(__name__)

def environment():
    hostname = socket.gethostname()
    log.debug( "envtypegrain hostname: " + hostname )  
    if( "salt2" ==  hostname ): 
        return { "envtype": "dev" } 
    else:
        return { "envtype": "ors" } 
    
if __name__ == "__main__":
 print( environment() )
