Set mine on minion:
  file.managed:
    - name: /etc/salt/minion.d/mine.conf
    - contents: |
       # define salt mine
       mine_interval: 5
       mine_functions:
         custom_host.info: []
         network.ip_addrs6:
           interface: ens2
         g_ip_addrs:
           - mine_function: grains.get
           - 'ip6_interfaces:ens2:0'
         n_ip_addrs:
           - mine_function: network.ip_addrs6
           - 'interface: ens2'


Restart minion:
  module.run:
    - name: cmd.run_bg
    - cmd: 'salt-call --local service.restart salt-minion'
    - onchanges: 
      - file: Set mine on minion

Edit hosts:
  file.managed:
    - name: /tmp/hosts
    - contents: |
       127.0.0.1       localhost

       # The following lines are desirable for IPv6 capable hosts
       ::1     ip6-localhost   ip6-loopback
       fe00::0 ip6-localnet
       ff00::0 ip6-mcastprefix
       ff02::1 ip6-allnodes
       ff02::2 ip6-allrouters
       ff02::3 ip6-allhosts
       {% for host in salt['mine.get'](tgt='*', tgt_type='glob', fun='custom_host.info').values() %}
       {{ host.ip }} {{ host.fqdn }} {{ host.hostname }}
       {%- if  host.hostname ==  salt['grains.get']('host') %}
       127.0.0.1 {{ host.fqdn }} {{ host.hostname }}
       {% endif %}
       {%- endfor %}
