#!/usr/bin/python3

import salt.modules.grains
import salt.modules.network

def info(interface='ens2'):
    fqdn = __salt__['network.get_fqdn']()
    hostname = __salt__['grains.get']('host')
    ip_addr = __salt__['network.ip_addrs6'](interface=interface)[0]
    return {
        'fqdn': fqdn,
        'hostname': hostname,
        'ip': ip_addr
    }

