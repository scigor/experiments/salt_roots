#!/bin/bash

pillar_file="$(echo $(pwd)/init.yq | sed s/salt/pillar/g)"
pillar_values=$(yq -r -o json . ${pillar_file} | tr '\n' ' ' | sed s/id/ft00/g)


echo salt zteam-dev02.zteam.fr state.sls account test=True | /bin/bash
echo "=> zteam-dev02.zteam.fr KO cause no pillar for minion on top.sls"
echo

echo salt zteam-dev02.zteam.fr state.sls account pillar=\'${pillar_values}\' test=True | /bin/bash
echo "=> zteam-dev02.zteam.fr OK cause pillar for minion on command_line"
echo

echo salt zteam-master02.zteam.fr state.sls account pillar=\'${pillar_values}\' test=True | /bin/bash
echo "=> zteam-master02.zteam.fr WARNING cause pillar for minion on command_line and top.sls"
echo
