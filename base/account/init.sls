
{% set passwd =  pillar['account']['passwd'] %}

{% for user_uid, user_params in pillar['account']['users'].items() %}
Create user {{user_uid}}:
  user.present:
    - name: {{user_params.name}}
    - password: {{passwd}}
    - uid: {{user_uid}}
{% endfor %}  
