installation nagios lib dependencies:
  pkg.installed:
    - pkgs:
        - unzip 
        - apache2
        - libapache2-mod-php
        - php-gd 
        - php
        - make
        - gcc 
        - build-essential 
        - libgd-dev
        - libssl-dev

download archive nagios:
 archive.extracted:
    - name: /tmp/nagios
    - source: https://github.com/NagiosEnterprises/nagioscore/releases/download/nagios-4.5.1/nagios-4.5.1.tar.gz
    - skip_verify: True
    - makeDirs: True
    - keep_source: False
    - enforce_toplevel: False
    - if_missing: /usr/local/nagios/bin/nagios

compile nagios:
  cmd.run: 
    - name: |
       cd nagios-4.5.1
       ./configure 
       make all && \
       make install-groups-users && \
       make install && \
       make install-daemoninit && \
       make install-commandmode && \
       make install-config && \
       make install-webconf
    - cwd: /tmp/nagios
    - onchanges:
      - archive: download archive nagios

download archive nagios-plugins:
 archive.extracted:
    - name: /tmp/nagios
    - source: https://github.com/nagios-plugins/nagios-plugins/releases/download/release-2.4.9/nagios-plugins-2.4.9.tar.gz
    - skip_verify: True
    - makeDirs: True
    - keep_source: False
    - enforce_toplevel: False

compile nagios-plugins:
  cmd.run: 
    - name: |
       cd nagios-plugins-2.4.9
       ./configure --with-nagios-user=nagios --with-nagios-group=nagios
       make && \
       make install
    - cwd: /tmp/nagios
    - onchanges:
      - archive: download archive nagios-plugins


