# note https://fr.linux-console.net/?p=21092

sudo apt update
sudo apt install wget unzip vim curl openssl build-essential libgd-dev libssl-dev libapache2-mod-php php-gd php apache2


NAGIOS_VER=$(curl -s https://api.github.com/repos/NagiosEnterprises/nagioscore/releases/latest|grep tag_name|cut -d '"' -f 4)
curl -SL https://github.com/NagiosEnterprises/nagioscore/releases/download/$NAGIOS_VER/$NAGIOS_VER.tar.gz | tar -xzf -

sudo make install-groups-users
sudo usermod -a -G nagios www-data

cd $NAGIOS_VER
sudo make install
sudo make install-daemoninit
sudo make install-commandmode
sudo make install-config

sudo make install-webconf
sudo a2enmod rewrite cgi
sudo systemctl restart apache2

sudo make install-exfoliation
sudo make install-classicui

sudo htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
New password: 
Re-type new password: 
Adding password for user nagiosadmin


VER=$( curl -s https://api.github.com/repos/nagios-plugins/nagios-plugins/releases/latest|grep tag_name|cut -d '"' -f 4|sed 's/release-//')
curl -SL https://github.com/nagios-plugins/nagios-plugins/releases/download/release-$VER/nagios-plugins-$VER.tar.gz | tar -xzf -

cd nagios-plugins-$VER
./configure --with-nagios-user=nagios --with-nagios-group=nagios
make
sudo make install

