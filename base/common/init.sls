installation lib apt:
  pkg.installed:
    - pkgs:
       - jq
       - tree

extraction yq:
  archive.extracted:
    - name: /tmp/
#    - source: salt://pkg/arch/yq_linux_amd64.tar.gz
    - source: https://github.com/mikefarah/yq/releases/download/v4.43.1/yq_linux_amd64.tar.gz
    - skip_verify: True
    - source_hash: https://github.com/mikefarah/yq/releases/download/v4.43.1/checksums
    - source_hash_name: yq_linux_amd64.tar.gz
    - keep_source: False
    - enforce_toplevel: False
    - if_missing: /usr/bin/yq

push yq:
  file.managed:
    - source: /tmp/yq_linux_amd64
    - name: /usr/bin/yq
    - mode: 755
    - user: root
    - group: root
    - onchanges:
      - archive: extraction yq
