installation packages nagios:
  pkg.installed:
    - pkgs:
       - nagios-nrpe-server
       - monitoring-plugins

#installation nagios lib dependencies:
#  pkg.installed:
#    - pkgs:
#       - gcc 
#       - make
#       - libssl-dev

# download archive nrpe:
#  archive.extracted:
#    - name: /tmp/
#    - source: https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-4.0.2/nrpe-4.0.2.tar.gz
#    - skip_verify: True
#    - keep_source: False
#    - enforce_toplevel: False
#    - if_missing: /usr/sbin/nrpe

#compile nrpe:
#  cmd.run: 
#    - name: |
#       ./configure  --disable-ssl
#       make all
#       make install-plugin
#       make install-daemon
#       make install-init
#    - cwd: /tmp/nrpe-4.0.2
#    - onchanges:
#      - archive: download archive nrpe

/etc/nagios/nrpe.cfg:
  file.replace:
    - pattern: 'allowed_hosts=127.0.0.1.*'
    - repl: 'allowed_hosts=127.0.0.1,::1,zteam-master01.zteam.fr'

nagios-nrpe-server:
  service.running:
    - enable: True
    - reload: True
