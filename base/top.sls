base:
  'salt_minions':
    - match: nodegroup
    - minion-mine

  'group:salt_nagios':
    - match: grain
    - common
    - npre

  'zteam-master02-n1':
    - nagios

  'zteam-master02-g1':
    - node-exporter
    - prometheus
    - grafana

  'zteam-dev06':
    - account
